##link

<a
className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>

##Route

 <BrowserRouter>
      <Link to="/login" className="App-link">Login</Link>
      <Routes>
        <Route path='/login' element={<Login />} />
      </Routes>
    </BrowserRouter> 