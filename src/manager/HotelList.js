import React, { useState, useEffect } from 'react';
import axios from 'axios';


function HotelList() {
  const [hotels, setHotels] = useState([]);

//   useEffect(() => {
//     fetch('/api/hotels')
//       .then(response => response.json())
//       .then(data => setHotels(data));
//   }, []);
useEffect(() => {
    axios.get('/api/hotels')
      .then(response => setHotels(response.data))
      .catch(error => console.error(error));
  }, []);
  
  // ...
  return (
    <div>
      <h1>Hotel List</h1>
      {hotels.map(hotel => (
        <div key={hotel.id}>
          <h2>{hotel.name}</h2>
          <p>{hotel.description}</p>
          <p>{hotel.price}</p>
          <img src={hotel.image} alt={hotel.name} />
        </div>
      ))}
    </div>
  );
  
}

export default HotelList;