import React from 'react';
import "./elements.css";
import axios from "axios";


class Login extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: '',
    };
  }

  handleEmailChange = (event) => {
    this.setState({ email: event.target.value });
  }

  handlePasswordChange = (event) => {
    this.setState({ password: event.target.value });
  }

  handleSubmit = async (event) => {
    event.preventDefault();
// the axios.post() method to send a POST request to the
// backend API with the user's credentials. If the API
// returns a JWT token, we store it in the session storage
// using sessionStorage.setItem(). We then update the header 
//component using this.props.UpdateHeader() and redirect to the
// secure route using this.props.history.push().//
    try {
      const response = await axios.post('/user/login', {
        email: this.state.email,
        password: this.state.password,
      });

      if (response.status === 200 && response.data.token) {
        // Store the JWT token in session storage
        sessionStorage.setItem('token', response.data.token);

        // Update the header component
        this.props.UpdateHeader(this.state.email);

        // Redirect to the secure route
        this.props.history.push('/secure');
      } else {
        // Clear the form and show an error message
        this.setState({
          email: '',
          password: '',
          error: 'Invalid username or password. Please try again.',
        });
      }
    } catch (error) {
      console.error(error);
      this.setState({
        email: '',
        password: '',
        error: 'An error occurred. Please try again later.',
      });
    }
  }


  render() {
    return (
      // <div>
      //   <div class="loginCard">
      //   <h1>Login</h1>
      //   <form onSubmit={this.handleSubmit}>
      //     <div>
      //       <label>Email:</label>
      //       <input type="email" value={this.state.email} onChange={this.handleEmailChange} />
      //     </div>
      //     <div>
      //       <label>Password:</label>
      //       <input type="password" value={this.state.password} onChange={this.handlePasswordChange} />
      //     </div>
      //     <button type="submit">Login</button>
      //   </form>
      //   </div>
      // </div>

      <div class="a">
        <h1>Login</h1>
        <form onSubmit={this.handleSubmit}>
          <div class="form-outline">

            <label class="form-label" for="form2Example1">Email address</label>
            <input type="email" class="form-control" value={this.state.email} onChange={this.handleEmailChange} />
          </div>

          <div class="form-outline">

            <label class="form-label" for="form2Example2">Password</label>
            <input type="password" value={this.state.password} onChange={this.handlePasswordChange} class="form-control" />
          </div>

          <div class="row mb-4">
            <div class="col d-flex justify-content-center">
           </div>
          </div>

          <button type="submit" class="btn btn-primary btn-block mb-4">Sign in</button>

          <div class="text-center">
            <p>Not a member? <a href="http://localhost:3000/components/Register">Register</a></p>
            <div class="col">
              <a href="#!">Forgot password?</a>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

export default Login;

