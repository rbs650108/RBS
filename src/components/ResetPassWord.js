import React, { useState } from 'react';

import "./elements.css";
const ResetPassWord = () => {
  const [email, setEmail] = useState('');
  const [newPassword, setNewPassword] = useState('');
  const [confirmNewPassword, setConfirmNewPassword] = useState('');
  const [message, setMessage] = useState('');

  const handleSubmit = (event) => {
    event.preventDefault();
    // validate email, newPassword and confirmNewPassword here
    // call API endpoint to reset password
    setMessage('Password reset successful!');
  };

  return (
    <div class="a" >
      
      <h1>Password Reset</h1>
      <form onSubmit={handleSubmit}>
        <div >
          <label htmlFor="email" class="form-label" for="form2Example1">Email:</label>
          <input
           class="form-control"
            type="email"
            name="email"
            value={email}
            onChange={(event) => setEmail(event.target.value)}
            required
          />
        </div>
        <br/>
        <div>
          <label htmlFor="newPassword" class="form-label" for="form2Example2">New Password:</label>
          <input class="form-control"
            type="password"
            name="newPassword"
            value={newPassword}
            onChange={(event) => setNewPassword(event.target.value)}
            minLength="4"
            maxLength="20"
            required
          />
        </div>
        <br/>
        <div>
          <label htmlFor="confirmNewPassword" class="form-label" for="form2Example2">Confirm New Password:</label>
          <input class="form-control"
            type="password"
            name="confirmNewPassword"
            value={confirmNewPassword}
            onChange={(event) => setConfirmNewPassword(event.target.value)}
            minLength="4"
            maxLength="20"
            required
          />
        </div>
        <div class="row mb-4">
            <div class="col d-flex justify-content-center">
           </div>
          </div>
        <button type="submit" class="btn btn-primary btn-block mb-4">Reset Password</button>
      </form>
      {message && <p>{message}</p>}
    </div>
  );
};

export default ResetPassWord;
