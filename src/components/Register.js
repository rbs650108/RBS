import axios from 'axios';
import React, { useState } from "react";

import "./elements.css";
import Alert from 'react-bootstrap/Alert';
const RegistrationForm = () => {
  const [formData, setFormData] = useState({
    name: "",
    phoneNumber: "",
    email: "",
    gender: "",
    password: "",
    confirmPassword: "",
  });

  const [errors, setErrors] = useState({});

  const handleChange = (event) => {
    setFormData({ ...formData, [event.target.name]: event.target.value });
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    const errors = validate();
    if (Object.keys(errors).length === 0) {
      // Make API call to send form data
      debugger;
      // fetch("localhost:7070/user/signup", {
      //   method: 'POST',
      //   body: JSON.stringify(formData),
      //   headers: {
      //     'Content-Type': 'application/json'
      //   }
      // })
      //   .then(response => response.json())
      //   .then(data => console.log(data))
      //   .catch(error => console.error(error))

        try {
          const response = axios.post('http://localhost:7070/user/signup', formData);
          console.log(response.data);
        } catch (error) {
          console.error(error);
        }
    } else {
      setErrors(errors);
    }
  };

  const validate = () => {
    const errors = {};
    if (!formData.name) {
      errors.name = "Name cannot be empty";
    }
    if (!formData.phoneNumber) {
      errors.phoneNumber = "Phone number cannot be empty";
    } else if (formData.phoneNumber.length !== 10) {
      errors.phoneNumber = "Phone number must be 10 digits";
    }
    if (!formData.email) {
      errors.email = "Email cannot be empty";
    } else if (!formData.email.includes("@")) {
      errors.email = "Invalid email";
    }
    if (!formData.password) {
      errors.password = "Password cannot be empty";
    } else if (formData.password.length < 4 || formData.password.length > 20) {
      errors.password =
        "Password must be between 4 and 20 characters long";
    }
    if (!formData.confirmPassword) {
      errors.confirmPassword = "Please confirm your password";
    } else if (formData.password !== formData.confirmPassword) {
      errors.confirmPassword = "Passwords do not match";
    }
    return errors;
  };

  return (
   <div class="a">
    
     <form onSubmit={handleSubmit}>
      <div class="form-outline">
        <label htmlFor="name" class="text form-label" for="form2Example1">Name:</label>
        <input  class="form-control"
          type="text"
          name="name"
          value={formData.name}
          onChange={handleChange}
        />
        {errors.name && <p className="error">{errors.name}</p>}
      </div>
      <div class="form-outline">
        <label htmlFor="phoneNumber" class=" text form-label" for="form2Example1">Phone No. :</label>
        <input  class="form-control"
          type="text"
          name="phoneNumber"
          value={formData.phoneNumber}
          onChange={handleChange}
        />
        {errors.phoneNumber && <p className="error">{errors.phoneNumber}</p>}
      </div>
      <div class="form-outline">
        <label htmlFor="email" class="text form-label" for="form2Example1">Email:</label>
        <input  class="form-control"
          type="text"
          name="email"
          value={formData.email}
          onChange={handleChange}
        />
        {errors.email && <p className="error">{errors.email}</p>}
      </div>
      <div class="form-outline">
        <label htmlFor="gender" class="text form-label" for="form2Example1">Gender:</label>
        <select name="gender" value={formData.gender} onChange={handleChange}  class="form-control">
          <option value=""></option>
          <option value="male">Male</option>
          <option value="female">Female</option>
          <option value="other">Other</option>
        </select>
      </div>
      <div class="form-outline">
        <label htmlFor="password" class="text form-label" for="form2Example1">Password:</label>
        <input  class="form-control"
          type="password"
          name="password"
          value={formData.password}
          onChange={handleChange}
        />
        {errors.password && <p className="error">{errors.password}</p>}
      </div>
      <div class="form-outline">
        <label htmlFor="confirmPassword" class="text form-label" for="form2Example1">Confirm Password:</label>
        <input  class="form-control"
          type="password"
          name="confirmPassword"
          value={formData.confirmPassword}
          onChange={handleChange}
        />
       </div>
       {errors.confirmPassword && <p className="error">{errors.confirmPassword}</p>}
      <button type="submit"  class="btn btn-primary btn-block mb-4">Register</button>
    </form>
   </div>
  );
}

export default RegistrationForm;