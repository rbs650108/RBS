import React from 'react';
import homeroom from './homeroom.jpg';
import "./elements.css";
function About() {
  return (
    <div>
      
      <h1>About Page</h1>
      <div class="last-text">
      <p>Welcome to our Online Room Booking Website! <br/>We are excited to 
        have you here and would like to take this opportunity to introduce ourselves.</p>
        </div>
 <div className='block'>
 <div className='img-text'>
      <div class="image">
      <img src={homeroom}alt="My Image" />
      </div>
      <div class="text">
       <p>Our company was founded with the goal of making travel easier and more 
        accessible for everyone. We understand how stressful it can be to plan a trip,
         especially when it comes to finding the perfect accommodation. That's why we created an online platform that simplifies the booking process 
        and makes it easy for travelers to find affordable and comfortable rooms.</p>
      </div>
    </div>

    <div className='img-text'>
      
      <div class="text">
       <p>At our Online Room Booking Website, we pride ourselves on offering a wide
         range of options to suit every traveler's needs. Whether you're looking for
        a luxurious hotel room or a cozy bed and breakfast, we've got you covered.
        We partner with reputable hotels and vacation rentals around the world to
        ensure that our guests have access to high-quality accommodations at competitive prices.</p>
      </div>
      <div class="image">
      <img src={homeroom}alt="My Image" />
      </div>
    </div>

    <div className='img-text'>
      <div class="image">
      <img src={homeroom}alt="My Image" />
      </div>
      <div class="text">
       <p>Our team consists of dedicated professionals who are passionate about travel and committed to providing exceptional customer service. 
        We work tirelessly to ensure that every guest has a smooth and enjoyable booking experience, and we're always available to answer any questions or concerns that you may have.</p>
      </div>
    </div>

    <div className='img-text'>
      
      <div class="text">
       <p>We believe that travel should be a fun and enriching experience, and we're committed to helping our guests make the most of their trips.
         Our Online Room Booking Website is designed to be user-friendly and intuitive, so you can easily find the perfect room for your next adventure.</p>
      </div>
      <div class="image">
      <img src={homeroom}alt="My Image" />
      </div>
    </div>

  
 </div>
  
    <div class="last-text">
       <p>Thank you for choosing our Online Room Booking Website. We look forward to helping you plan your next trip!</p>
      </div>
    </div>
    
  );
}

export default About;
