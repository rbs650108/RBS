import { BrowserRouter, Routes, Route, Link } from 'react-router-dom';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import Home from './components/Home';
import Login from './components/Login';
import About from './components/About';
import Contact from './components/Contact';
import ResetPassWord from './components/ResetPassWord';
import Register from './components/Register';
import HotelForm from './manager/HotelForm';

function App() {
  return (

    <div className="App" >
   
      <BrowserRouter>
        <div class="App-navbar">
          
        <Link to="/components/Home" className="App-navlink">STRANGERX.IN </Link>
        <Link to="/components/About" className="App-navlink">About</Link>
        <Link to="/components/Contact" className="App-navlink">Contact</Link>
        <Link to="/components/login" className="App-navlink">Login</Link>
        <Link to="/components/ResetPassWord" className="App-navlink">ResetPassWord</Link>
        <Link to="/components/Register" className="App-navlink">Register</Link>
        </div>
        <hr></hr>
        <Routes>
          <Route path='/components/Home' element={<Home />} />
          <Route path='/components/About' element={<About />} />
          <Route path='/components/Contact' element={<Contact />} />
          <Route path='/components/login' element={<Login />} />
          <Route path='/components/ResetPassWord' element={<ResetPassWord />} />
          <Route path='/components/Register' element={<Register />} />
        </Routes>
      </BrowserRouter>

      
      <BrowserRouter>
        <div class="App-navbar">
          
        <Link to="/manager/HotelForm" className="App-navlink">HotelForm</Link>
        </div>
        <hr></hr>
        <Routes>
          <Route path='/manager/HotelForm' element={<HotelForm />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
